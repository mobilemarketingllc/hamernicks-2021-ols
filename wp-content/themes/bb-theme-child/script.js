(function($) {
    $('.advSearch .fl-icon').unbind('click').click(function(e) {
        $('.searchHeader').slideToggle();
    })
    var $menu = $('.advSearch .fl-icon, .searchHeader');
    $(document).mouseup(function (e) {
        if (!$menu.is(e.target) && $menu.has(e.target).length === 0 && $('.searchHeader').css('display') == 'block') {
            e.preventDefault()
            $('.searchHeader').slideUp();
        }
    });
})(jQuery);